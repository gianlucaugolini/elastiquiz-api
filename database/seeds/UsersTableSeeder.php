<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        User::truncate();
        $users = [
            [
                'email'     => 'gianluca.ugolini@gmail.com',
                'password'  => Hash::make( 'laziolazio25' ),
                'name'      => 'Gianluca Ugolini',
            ],
            [
                'email'     => 'pogo2k@libero.it',
                'password'  => Hash::make( 'elastiquiz' ),
                'name'      => 'Enrico Maria Castelli',
            ],
            [
                'email'     => 'the.original.ultralord@gmail.com',
                'password'  => Hash::make( 'elastiquiz' ),
                'name'      => 'Andrea Rosati',
            ],
            [
                'email'     => 'saragemma108@gmail.com',
                'password'  => Hash::make( 'elastiquiz' ),
                'name'      => 'Sara Gemma',
            ],
        ];

        foreach ( $users as $u => $user ) {
            User::create( $user );
        }

    }
}
