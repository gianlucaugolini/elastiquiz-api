<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuizResult extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'quiz_result';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'score'
    ];


    /**
     * The User for the Quiz Results
     */
    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

}
