<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'feedback';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'rating', 'text'
    ];


    /**
     * The User of the Feedback
     */
    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

}
