<?php

namespace App\Http\Controllers;

use App\Models\QuizResult;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RankingController extends Controller {
    /**
     * Create a new RankingController instance.
     *
     * @return void
     */
    public function __construct() { }

    /**
     * Saves Quiz Results.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all( Request $request ) {
        $quiz_results = QuizResult::with( 'user' )->orderBy( 'score', 'desc' )->limit( 100 )->get();


        return response()->json( [ 'data' => $quiz_results ],200 );

    }
}