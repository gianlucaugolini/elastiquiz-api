<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller {
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        // $this->middleware('auth:api', [ 'except' => [ 'login', 'authenticate'] ] );
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login( Request $request ) {

        $credentials = request( [ 'email', 'password' ] );

        try {

            if ( !$token = JWTAuth::attempt( $credentials ) ) {

                return response()->json( [ 'error' => 'invalid_credentials' ], 400 );

            }

        } catch ( JWTException $e ) {

            return response()->json( [ 'error' => 'could_not_create_token' ], 500 );

        }

        return $this->respondWithToken( $token );

    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me() {

        return response()->json( auth()->user() );

    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {

        auth()->logout();

        return response()->json( [ 'message' => 'Successfully logged out' ] );

    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {

        return $this->respondWithToken( auth()->refresh() );

    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken( $token ) {
        return response()->json([
            'access_token'  => $token,
            'token_type'    => 'bearer',
            'expires_in'    => auth()->factory()->getTTL() * 60
        ]);
    }

    public function getAuthenticatedUser() {

        try {

            if ( !$user = JWTAuth::parseToken()->authenticate() ) {

                return response()->json(['user_not_found'], 404);

            }

        } catch ( Tymon\JWTAuth\Exceptions\TokenExpiredException $e ) {

            return response()->json(['token_expired'], $e->getStatusCode() );

        } catch ( Tymon\JWTAuth\Exceptions\TokenInvalidException $e ) {

            return response()->json(['token_invalid'], $e->getStatusCode() );

        } catch ( Tymon\JWTAuth\Exceptions\JWTException $e ) {

            return response()->json(['token_absent'], $e->getStatusCode() );

        }

        return response()->json( compact('user' ) );
    }

    public function register( Request $request ) {

        $validator = Validator::make( $request->all(), [
            'name'      => 'required|string|max:255|unique:users,name',
            'email'     => 'required|string|email|max:255|unique:users,email',
            'password'  => 'required|string|min:6',
        ] );

        if ( $validator->fails() ){
            return response()->json( $validator->errors()->toJson(), 400 );
        }

        $user = User::create( [
            'name'      => $request->get( 'name' ),
            'email'     => $request->get( 'email' ),
            'password'  => Hash::make( $request->get( 'password' ) ),
        ]);

        $token = JWTAuth::attempt( request( [ 'email', 'password' ] ) );

        return response()->json( compact( 'user','token' ),201 );
    }
}