<?php

namespace App\Http\Controllers;

use App\Helpers\CommonHelpers;
use App\Models\Feedback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeedbacksController extends Controller {
    /**
     * Create a new QuizController instance.
     *
     * @return void
     */
    public function __construct() {}

    /**
     * Saves Quiz Results.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function save( Request $request ) {


        $request_data = $request->json()->all();

        $rating = $request_data['rating'];
        $text = $request_data['text'];


        try {

            $user = auth()->user();
            $feedback = new Feedback();
            $feedback->rating = $rating;
            $feedback->text = $text;
            $saved = $user->feedbacks()->save( $feedback );


        } catch ( \Exception $e ) {

            return response()->json( [ 'error' => 'could_not_save_feedback' ], 500 );

        }

        $sentence = "easter egg";

        if( preg_match("#\b" . $sentence . "\b#i", $text ) ){
            $easter_egg = true;
        } else {
            $easter_egg = false;
        }

        return response()->json( [ 'saved' => true, 'ee' => $easter_egg ],201 );


    }
}