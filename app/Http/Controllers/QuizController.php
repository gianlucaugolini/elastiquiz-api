<?php

namespace App\Http\Controllers;

use App\Models\QuizResult;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuizController extends Controller {
    /**
     * Create a new QuizController instance.
     *
     * @return void
     */
    public function __construct() {}

    /**
     * Saves Quiz Results.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function save( Request $request ) {


        $request_data = $request->json()->all();

        $score = $request_data['score'];


        try {

            $user = auth()->user();
            $quiz_result = new QuizResult();
            $quiz_result->score = $score;
            $saved = $user->quizResults()->save( $quiz_result );


        } catch ( \Exception $e ) {

            return response()->json( [ 'error' => 'could_not_save_score' ], 500 );

        }

        return response()->json( [ 'saved' => true ],201 );


    }
}