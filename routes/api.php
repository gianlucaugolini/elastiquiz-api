<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([

    'middleware' => [
        'api',
        'jwt.auth'
    ],
    'prefix' => 'auth'

], function ( $router ) {

    Route::get( 'logout', 'AuthController@logout' );
    Route::post( 'refresh', 'AuthController@refresh' );
    Route::post( 'me', 'AuthController@me' );
    Route::get( 'user', 'AuthController@getAuthenticatedUser' );

});

Route::group([

    'middleware' => [
        'api'
    ],
    'prefix' => 'auth'

], function ( $router ) {

    Route::post( 'login', 'AuthController@login' );
    Route::post( 'register', 'AuthController@register' );

});

Route::group([

    'middleware' => [
        'api',
        'jwt.auth'
    ],
    'prefix' => 'quiz'

], function ( $router ) {

    Route::post( 'save', 'QuizController@save' );

});

Route::group([

    'middleware' => [
        'api',
        'jwt.auth'
    ],
    'prefix' => 'ranking'

], function ( $router ) {

    Route::get( 'all', 'RankingController@all' );

});

Route::group([

    'middleware' => [
        'api',
        'jwt.auth'
    ],
    'prefix' => 'feedback'

], function ( $router ) {

    Route::post( 'save', 'FeedbacksController@save' );

});